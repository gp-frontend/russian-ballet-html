(function ($) {

  /*----------------------------------------
  FOR SVG SPRITE
----------------------------------------*/
  svg4everybody({});

  /*----------------------------------------
   TRANSITION SCROLL
 ----------------------------------------*/
  $('.link-scroll').on('click', function (e) {
    // e.preventDefault();
    var anchor = $(this),
        headerHeight = $(".header").height();

    $('html, body').stop().animate({
      scrollTop: $(anchor.attr('href')).offset().top - headerHeight
    }, 1000)
  });

  //WIDTH SCROLL BAR
  var scrollBar = document.createElement('div');

  scrollBar.style.overflowY = 'scroll';
  scrollBar.style.width = '50px';
  scrollBar.style.height = '50px';

  // при display:none размеры нельзя узнать
  // нужно, чтобы элемент был видим,
  // visibility:hidden - можно, т.к. сохраняет геометрию
  scrollBar.style.visibility = 'hidden';

  document.body.appendChild(scrollBar);
  var scrollWidth = scrollBar.offsetWidth - scrollBar.clientWidth;
  document.body.removeChild(scrollBar);

  /*----------------------------------------
  POPUPS / MODALS
  ----------------------------------------*/
  (function () {

    var ESC_KEYCODE = 27;

    var popup = $(".popup"),
      popupOpen = $("[data-popup='open']"),
      popupClose = $("[data-popup='close']"),
      popupOverlay = $(".popup__overlay");

    /* POPUP FUNCTIONS */
    $.fn.gpPopup = function( method ) {

      if ( methods[method] ) {
        return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
      } else if ( typeof method === 'object' || ! method ) {
        return methods.show.apply( this, arguments );
      } else {
        $.error( 'Метод с именем ' +  method + ' не существует' );
      }

    };

    // alert( scrollWidth );

    /* POPUP METHODS */
    var methods = {
      //show popup
      show : function( options ) {
        return this.each(function(){

          popup.removeClass('popup--open');
          $(this).addClass('popup--open');
          $('body').addClass('page--locked');

          if(scrollWidth > 0){
            $('body').css('padding-right', scrollWidth);
            $('header').css('padding-right', scrollWidth);
          }
        });
      },
      //hide all popups
      hide : function( ) {

        return this.each(function(){

          if (popup.hasClass('popup--open')) {
            popup.removeClass('popup--open');
            $('body').removeClass('page--locked').css('padding-right', '');
            $('header').css('padding-right', '');
          }

        })

      }
    };

    /* OPEN */
    function openPopup(e){
      e.preventDefault();
      var id = $(this).attr('data-popup-id');
      $(id).gpPopup();
    }

    /* CLOSE */
    function closePopup(e){
      e.preventDefault();
      popup.gpPopup('hide');
    }

    /* ADD CLICK FUNCTION */
    popupOpen.click(openPopup);
    popupClose.click(closePopup);
    popupOverlay.click(closePopup);

    /* PRESS ESC BUTTON */
    $(document).keydown(function(evt) {
      if( evt.keyCode === ESC_KEYCODE ) {
        popup.gpPopup('hide');
        return false;
      }
    });
  })();

  /*----------------------------------------
   MENU
  ----------------------------------------*/
  var menu = $("#menu"),
      buttonOpenMenu = $(".js-button-open-menu"),
      buttonCloseMenu = $(".js-button-close-menu");

  $('<div>', { class: 'page__overlay'}).appendTo('.header');

  function openMenu() {
    menu.addClass("menu--open");
    $(".page__overlay").addClass("page__overlay--show");
    $("body").addClass("page--locked");

    if(scrollWidth > 0){
      $('body').css('padding-right', scrollWidth);
    }
  }

  function closeMenu() {
    menu.removeClass("menu--open");
    $(".page__overlay").removeClass("page__overlay--show");
    $("body").removeClass("page--locked").css("padding-right", "");
  }

  buttonOpenMenu.on("click", openMenu);
  buttonCloseMenu.on("click", closeMenu);
  $(".menu__link").on("click", closeMenu);
  $(".page__overlay").on("click", closeMenu);


  /*----------------------------------------
   INPUT MASK
  ----------------------------------------*/
  $(".js-mask-number").mask("0#");
  $(".js-mask-phone").mask("+7 (000) 000-00-00");
  $(".js-mask-time").mask("00 : 00");
  $(".js-mask-date").mask("00.00.0000");

  /*----------------------------------------
   CAROUSEL
  ----------------------------------------*/
  $(document).ready(function() {

    $('.js-carousel').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      centerMode: true,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            dots: true,
            arrows: false
          }
        },
      ]
    });

    $('.js-youtube-carousel').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      centerMode: true,
      variableWidth: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            dots: true,
            arrows: false
          }
        },
      ]
    });
  });

    // slider
    var $compositions = $('.js-carousel-compositions');

    settings_slider = {
      slidesToShow: 2,
      slidesToScroll: 1,
      dots: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1
          }
        },
      ]
    };

    // slick on mobile
    function slick_on_mobile(slider, settings){

      $(window).on('load resize', function() {

        if ($(window).width() > 1023 - scrollWidth) {

          if (slider.hasClass('slick-initialized')) {
            slider.slick('unslick');
          }
          return
        }

        if (!slider.hasClass('slick-initialized')) {
          return slider.slick(settings);
        }
      });
    }

    slick_on_mobile( $compositions, settings_slider);


  /*----------------------------------------
    Range Slider
  ----------------------------------------*/
  $(function () {

    var $range = $(".js-range-slider");

    $range.ionRangeSlider();
  });

  /*----------------------------------------
   CALCULATOR
  ----------------------------------------*/

  var calcCost = $("#range-cost"),
      calcCostValue = calcCost.data("from"),
      calcQuantity= $("#range-quantity"),
      calcQuantityValue = calcQuantity.data("from");

  function calculateSum(a, b) {
    var sum = Math.round((a * b) - (b / 12 * 8 * 1200) - (b / 12 * 8 * 1000) - (b / 12 * 8 * 500));

    $(".js-calc-sum").text(sum);
  }

  calculateSum(calcCostValue, calcQuantityValue);

  $(".js-range-slider").on("change", function () {
    var val1 = calcCost.prop("value");
    var val2 = calcQuantity.prop("value");

    calculateSum(val1, val2);
  });

  /*----------------------------------------
   VALIDATE FORMS
  ----------------------------------------*/

  $('#form-call').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function () {
      $('#popup-thanks').gpPopup();
    }
  });

  $('#form-cost').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function () {
      $('#popup-thanks').gpPopup();
    }
  });

  $('#form-advice').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function () {
      $('#popup-thanks').gpPopup();
    }
  });

  $('#form-order').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function () {
      $('#popup-thanks').gpPopup();
    }
  });

  $(".js-modal-btn").modalVideo();


  /* SCROLL SPY */
  $(function () {

    var link = $('.js-menu a');

    // Run the scrNav when scroll
    $(window).on('scroll', function () {
      scrNav()
    });

    // scrNav function
    // Change active dot according to the active section in the window
    function scrNav () {
      var sTop = $(window).scrollTop();

      $('section').each(function () {

        var id = $(this).attr('id'),
          offset = $(this).offset().top - 1,
          height = $(this).height();

        if (sTop >= offset && sTop < offset + height) {
          link.removeClass('menu__link--active');
          $('.js-menu').find('a[href="#' + id + '"]').addClass('menu__link--active');
        }
      })
    }

    scrNav()
  });

  var num = 0;

  $(window).scroll(function() {

    var oTop = $('.js-count').offset().top - window.innerHeight;

    if (num == 0 && $(window).scrollTop() > oTop) {

      $('.js-count').each(function() {

        var $this = $(this),
          countTo = $this.attr('data-count');

        $({
          countNum: $this.text()
        }).animate({
            countNum: countTo
          },

          {

            duration: 2000,
            easing: 'swing',
            step: function() {
              $this.text(Math.floor(this.countNum));
            },
            complete: function() {
              $this.text(this.countNum);
              //alert('finished');
            }

          });
      });
      num = 1;
    }

  });

})(jQuery);
