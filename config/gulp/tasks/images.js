let imagesPaths = {
  'srcCopy': 'src/images/**/*.{jpg,png,JPEG,gif,jpeg,svg}',
  'srcGif': 'src/images/**/*.gif',
  'srcCompress': 'src/images/**/*.{jpg,png,JPEG,jpeg}',
  'srcSVG': 'src/images/**/*.svg',
  'dev': 'build/images',
};

module.exports = () => {

  $.gulp.task('images:copy', () => {

    return $.gulp.src(imagesPaths.srcCopy)
      .pipe($.gulp.dest(imagesPaths.dev))
  });

  $.gulp.task('images:compress', () => {

    return $.gulp.src(imagesPaths.srcCompress)

      .pipe($.plugins.tinypng('v7ybahawsh7pIWPB0vBkP6p56MLf4Wc6'))

      .pipe($.gulp.dest(imagesPaths.dev))
  });

  $.gulp.task('images:gif', () => {
    return $.gulp.src(imagesPaths.srcGif)
      .pipe($.gulp.dest(imagesPaths.dev))
  });

  $.gulp.task('images:svg', () => {

    return $.gulp.src(imagesPaths.srcSVG)

      .pipe($.plugins.svgmin({

        js2svg: {
          pretty: true
        }
      }))

      .pipe($.gulp.dest(imagesPaths.dev))
  });
};
