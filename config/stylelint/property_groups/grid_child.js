"use strict";

module.exports = {
  "groupName": "Grid child rules",
  "order": "flexible",
  "properties": [
    "grid-row",
    "grid-row-start",
    "grid-row-end",
    "grid-column",
    "grid-column-start",
    "grid-column-end",
    "grid-area"
  ]
}
