"use strict";

module.exports = {
  "groupName": "Margin",
  "properties": [
    "margin",
    "margin-top",
    "margin-right",
    "margin-bottom",
    "margin-left"
  ]
}
