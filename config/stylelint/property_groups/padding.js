"use strict";

module.exports = {
  "groupName": "Padding",
  "properties": [
    "padding",
    "padding-top",
    "padding-right",
    "padding-bottom",
    "padding-left"
  ]
}
